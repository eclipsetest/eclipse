import java.io.File;

public class FileListService {
	
	public String[] listInFolder(String folder)throws Exception{
		
		File dir = new File(folder);
		
		return dir.list();
		
	}

}
