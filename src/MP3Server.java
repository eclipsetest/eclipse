import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class MP3Server {

	public static void main(String[] args) throws Exception {
		
		FileListService service = new FileListService();
		ServerSocket server = new ServerSocket(8080);
		
		while(true) {
			Socket socket = server.accept();
			
			new Thread() {
				public void run(){
					try {
						//확장자 출력하기
						Scanner inScanner = new Scanner(socket.getInputStream());
						OutputStream out = socket.getOutputStream();
						
						String str = inScanner.nextLine();
						String[] arr = str.split(" ");
						String fileName = arr[1].substring(1);
						System.out.println(fileName);
						
						String suffix = fileName.substring(fileName.lastIndexOf(".")+1);
						System.out.println(suffix);
						
						//리스트 추가
						if (suffix.equals("list.html")){
							String[] fileNames = service.listInFolder("C:\\zzz");
							StringBuilder builder = new StringBuilder("<ul>");
							
							for (String fn : fileNames){
										builder.append("<li><a href='" + fn + "' target='zero'>"+fn+"</a></li>");
							}
							builder.append("</ul>");
							builder.append("<iframe name='zero' style='width:100%;height:100%;'></iframe>");
							String strArr = builder.toString();
							byte[] data = strArr.getBytes();
							
							out.write(new String("HTTP/1.1 200 OK\r\n").getBytes()); 
							out.write(new String("Cache-Control: private\r\n").getBytes());
							out.write(new String("Content-Length: "+data.length+"\r\n").getBytes());
							out.write(new String("Content-Type: text/html\r\n\r\n").getBytes());

							out.write(data);
							
							return;
						}
						File file = new File("C:\\zzz"+fileName);
						InputStream fin = new FileInputStream(file);
						
						long fileSize = file.length();
						
						out.write(new String("HTTP/1.1 200 OK\r\n").getBytes());
						out.write(new String("Cache-Control: private\r\n").getBytes());
						out.write(new String("Content-Length: "+fileSize+"\r\n").getBytes()); //파일 사이즈를 입력

						if(suffix.equals("mp3")) { //MP3 파일을 브라우저에 실행
							out.write(new String("Content-Type: audio/mpeg\r\n\r\n").getBytes());
						} else if(suffix.equals("html")){ //html 파일을 브라우저에 실행
							out.write(new String("Content-Type: text/html\r\n\r\n").getBytes());
						} else if(suffix.equals("jpg")){ //jpg 파일을 브라우저에 실행
							out.write(new String("Content-Type: image/jpeg\r\n\r\n").getBytes());
						}
						
						
						
					} catch (Exception e) {
						
					}{
						
					}
				}
			}.start();
		}
		

	}

}
